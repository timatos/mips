
#--------------------------------------------------------------------
# variaveis estaticamente alocadas (no segmento de dados)
#--------------------------------------------------------------------

.data

ARR:
  .word 0 4 8 4 0 -4 -8 -4  # array com valores da onda triangular

N:
  .word 8  # qtde de elementos no array (pontos da onda)

#--------------------------------------------------------------------



#--------------------------------------------------------------------
# codigo de programa a partir daqui
#--------------------------------------------------------------------

.text

# temos logo abaixo uma chamada para:
# soma = SOMA_VETOR(int* array, int size)

la $a0, ARR  # primeiro argumento

lw $a1, N  # segundo argumento

jal SOMA_VETOR  # chama a funcao


b FIM  # encerra o programa

#--------------------------------------------------------------------



#--------------------------------------------------------------------
# soma os elementos do vetor apontado por $a0 que contem $a1 elementos
# $v0 = SOMA_VETOR($a0, $a1)
#--------------------------------------------------------------------
SOMA_VETOR:

  li $t0, 0  # soma = 0
  
  li $t1, 0  # i = 0
  
  LOOP:
    beq $t1, $a1, FIM_LOOP
    
    mul $t2, $t1, 4  # offset para indexao do vetor de int na memoria
    
    add $t3, $a0, $t2  # ponteiro para array[i]
    
    lw $t4, 0($t3)  # carrega valor de array[i] para $t4
    
    add $t0, $t0, $t4  # soma = soma + array[i]
    
    addi $t1, $t1, 1
    
    b LOOP
    
  FIM_LOOP:
  
  move $v0, $t0  # retorna o valor da soma
  
  jr $ra
#--------------------------------------------------------------------



#--------------------------------------------------------------------

FIM:  # ponto de encerramento do programa

#--------------------------------------------------------------------
